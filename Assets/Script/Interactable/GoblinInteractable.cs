using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinInteractable : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKey(KeyCode.F))
        {
            GetComponentInChildren<DialogueTrigger>().TriggerDialogue();
        }
    }
}
