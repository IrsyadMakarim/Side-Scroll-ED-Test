using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionalChestInteractable : MonoBehaviour
{
    public GameObject ChestInventory;
    public GameObject ChestTransferBtn;
    public OptionalChestManager chestManager;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKey(KeyCode.F))
        {
            if (chestManager.Loots.Count == 0)
            {
                GetComponentInChildren<DialogueTrigger>().TriggerDialogue();
            }
            else
            {
                chestManager.ListItems();
                ChestInventory.SetActive(true);
                ChestTransferBtn.SetActive(true);
            }
        }
    }
}
