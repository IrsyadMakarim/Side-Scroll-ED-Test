using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestInteractable : MonoBehaviour
{
    public GameObject ChestInventory;
    public GameObject ChestTransferBtn;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Input.GetKey(KeyCode.F))
        {
            ChestManager.Instance.ListItems();
            ChestInventory.SetActive(true);
            ChestTransferBtn.SetActive(true);           
        }
    }
}
