using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    public Item item;
    public GameObject hint;

    public void Pickup()
    {
        InventoryManager.Instance.Add(item);
        Destroy(gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            //hint.SetActive(true);
            Debug.Log("Player pass");
            if (Input.GetKey(KeyCode.F))
            {               
                Pickup();
                GetComponentInChildren<DialogueTrigger>().TriggerDialogue();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            //hint.SetActive(false);
        }
    }
}
