using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    public Item item;

    private void Start()
    {
        if (InventoryManager.Instance.CheckItemsInInventory(item) || ChestManager.Instance.CheckItemsInInventory(item))
        {
            Destroy(gameObject);
        }
    }
}
