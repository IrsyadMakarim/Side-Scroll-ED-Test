using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;
    public float jumpForce;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public float checkRadius;
    public int maxJumpCount;
    public GameObject hint;
    public Animator animator;

    private Rigidbody2D rb;
    private float moveDir;
    private bool facingRight;
    private bool isJumping;
    private bool isGrounded;
    private int jumpCount;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        animator.SetBool("IsIdle", true);
        animator.SetBool("IsRun", false);
        facingRight = true;
        isJumping = false;
        animator.SetBool("IsJump", isJumping);
        jumpCount = maxJumpCount;
        this.gameObject.transform.position = PositionManager.Instance.GetPos();
    }

    private void Update()
    {
        moveDir = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && jumpCount > 0)
        {
            isJumping = true;
            animator.SetBool("IsJump", isJumping);
            animator.SetBool("IsIdle", false);
        }

        if (moveDir > 0 && !facingRight)
        {
            FlipSprite();
        }
        else if (moveDir < 0 && facingRight)
        {
            FlipSprite();
        }
    }

    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, groundLayer);
        if (isGrounded)
        {
            jumpCount = maxJumpCount;
            animator.SetBool("IsIdle", false);
        }

        rb.velocity = new Vector2(moveDir * moveSpeed, rb.velocity.y);

        if (rb.velocity.x > 0 || rb.velocity.x < 0)
        {
            animator.SetBool("IsRun", true);
            animator.SetBool("IsIdle", false);
        }

        if (rb.velocity.x == 0)
        {
            animator.SetBool("IsRun", false);
            animator.SetBool("IsIdle", true);
        }

        if (isJumping) 
        {
            rb.AddForce(new Vector2(0f, jumpForce));
            jumpCount--;
        } 

        isJumping = false;
        animator.SetBool("IsJump", isJumping);
    }

    private void FlipSprite()
    {
        facingRight = !facingRight;

        transform.Rotate(0f, 180f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        hint.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        hint.SetActive(false);
    }
}
