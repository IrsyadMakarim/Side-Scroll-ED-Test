using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue[] Dialogue;

    private bool isTriggered = false;

    public void TriggerDialogue()
    {
        if (!isTriggered && Dialogue.Length == 1)
        {
            DialogueManager.Instance.StartDialogue(Dialogue[0]);           
        }

        if (!isTriggered && Dialogue.Length == 2)
        {
            DialogueManager.Instance.StartDialogue(Dialogue[0]);
            isTriggered = true;
        }
        else if (isTriggered && Dialogue.Length == 2)
        {
            DialogueManager.Instance.StartDialogue(Dialogue[1]);
        }
    }
}
