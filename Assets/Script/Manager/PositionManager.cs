using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionManager : MonoBehaviour
{
    private static PositionManager instance;
    public static PositionManager Instance { get { return instance; } }

    private Vector2 playerPos;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void SetPos(Vector2 pos)
    {
        playerPos = pos;
    }

    public Vector2 GetPos()
    {
        return playerPos;
    }
}
