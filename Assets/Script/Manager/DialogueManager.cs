using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    private static DialogueManager instance;
    public static DialogueManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<DialogueManager>();
            }

            return instance;
        }
    }

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;
    public Image characterPotraitOne;
    public Image characterPotraitTwo;

    public Animator dialogueBoxAnimator;

    public Queue<string> DialogueName;
    public Queue<string> Sentences;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    private void Start()
    {
        Sentences = new Queue<string>();
        DialogueName = new Queue<string>();
        characterPotraitOne.gameObject.SetActive(false);
        characterPotraitTwo.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            DisplayNextSentence();
        }
    }

    public void StartDialogue(Dialogue dialogue)
    {
        DialogueName.Clear();
        Sentences.Clear();

        dialogueBoxAnimator.SetBool("IsOpen", true);

        GeneratePotrait(dialogue);

        foreach (string name in dialogue.Name)
        {
            DialogueName.Enqueue(name);
        }

        foreach (string sentence in dialogue.Sentences)
        {
            Sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (Sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = Sentences.Dequeue();
        string name = DialogueName.Dequeue();
        dialogueText.text = sentence;
        nameText.text = name;
    }

    public void GeneratePotrait(Dialogue dialogue)
    {
        if (dialogue.CharacterPotraitOne != null)
        {
            characterPotraitOne.gameObject.SetActive(true);
            characterPotraitOne.sprite = dialogue.CharacterPotraitOne;
        }

        if (dialogue.CharacterPotraitTwo != null)
        {
            characterPotraitTwo.gameObject.SetActive(true);
            characterPotraitTwo.sprite = dialogue.CharacterPotraitTwo;
        }
    }

    public void EndDialogue()
    {
        dialogueBoxAnimator.SetBool("IsOpen", false);
        characterPotraitOne.gameObject.SetActive(false);
        characterPotraitTwo.gameObject.SetActive(false);                                                     
    }
}
