using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestManager : MonoBehaviour
{
    private static ChestManager instance;
    public static ChestManager Instance { get { return instance; } }

    public List<Item> ChestItems = new List<Item>();

    public Transform ItemContent;
    public GameObject InventoryItem;

    public InventoryItemController[] InventoryItems;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void Add(Item item)
    {
        ChestItems.Add(item);
    }

    public void Remove(Item item)
    {
        ChestItems.Remove(item);
    }

    public void ListItems()
    {
        ClearList();

        foreach (var item in ChestItems)
        {
            GameObject obj = Instantiate(InventoryItem, ItemContent);
            var itemName = obj.transform.Find("ItemName").GetComponent<TMPro.TextMeshProUGUI>();
            var itemIcon = obj.transform.Find("ItemIcon").GetComponent<Image>();

            itemIcon.sprite = item.icon;
            itemName.text = item.itemName;
        }

        SetInventoryItems();
    }

    public void ListInventoryItems()
    {
        InventoryManager.Instance.ListItems();
    }

    public void ClearList()
    {
        foreach (Transform item in ItemContent)
        {
            Destroy(item.gameObject);
        }
    }

    public void SetInventoryItems()
    {
        InventoryItems = ItemContent.GetComponentsInChildren<InventoryItemController>();

        for (int i = 0; i < ChestItems.Count; i++)
        {
            InventoryItems[i].AddItem(ChestItems[i]);
        }
    }

    public bool CheckItemsInInventory(Item item)
    {
        if (ChestItems.Contains(item))
        {
            return true;
        }

        return false;
    }

    public void TransferItems()
    {
        for (int i = 0; i < InventoryManager.Instance.Items.Count; i++)
        {
            Add(InventoryManager.Instance.Items[i]);
        }

        InventoryManager.Instance.Items.Clear();
    }
}
