using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeManager : MonoBehaviour
{
    public string sceneId;
    public Vector2 position;

    public void ChangeScene()
    {      
        SceneManager.LoadScene(sceneId);
        PositionManager.Instance.SetPos(position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ChangeScene();
    }
}
