using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionalChestManager : MonoBehaviour
{
    public List<Item> Loots = new List<Item>();

    public Transform ItemContent;
    public GameObject InventoryItem;

    public InventoryItemController[] InventoryItems;

    private void Start()
    {
        foreach (var item in Loots)
        {
            if (InventoryManager.Instance.CheckItemsInInventory(item) || ChestManager.Instance.CheckItemsInInventory(item))
            {
                Loots.Clear();
            }
        }       
    }

    public void Remove(Item item)
    {
        Loots.Remove(item);
    }

    public void ListItems()
    {
        ClearList();

        foreach (var item in Loots)
        {
            GameObject obj = Instantiate(InventoryItem, ItemContent);
            var itemName = obj.transform.Find("ItemName").GetComponent<TMPro.TextMeshProUGUI>();
            var itemIcon = obj.transform.Find("ItemIcon").GetComponent<Image>();

            itemIcon.sprite = item.icon;
            itemName.text = item.itemName;
        }

        SetInventoryItems();
    }

    public void ListInventoryItems()
    {
        InventoryManager.Instance.ListItems();
    }

    public void ClearList()
    {
        foreach (Transform item in ItemContent)
        {
            Destroy(item.gameObject);
        }
    }

    public void SetInventoryItems()
    {
        InventoryItems = ItemContent.GetComponentsInChildren<InventoryItemController>();

        for (int i = 0; i < Loots.Count; i++)
        {
            InventoryItems[i].AddItem(Loots[i]);
        }
    }

    public void TransferItemsToInventory()
    {
        for (int i = 0; i < Loots.Count; i++)
        {
            InventoryManager.Instance.Add(Loots[i]);
        }

        Loots.Clear();
    }
}
